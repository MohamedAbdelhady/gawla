package it_geeks.info.gawla_app.repository;

public enum RequestsActions {
    login,
    loginOrRegisterWithSocial,
    register,
}
