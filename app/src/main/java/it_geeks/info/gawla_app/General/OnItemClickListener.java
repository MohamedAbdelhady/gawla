package it_geeks.info.gawla_app.general;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view, int position);
}