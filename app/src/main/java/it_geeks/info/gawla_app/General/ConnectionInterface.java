package it_geeks.info.gawla_app.general;

public interface ConnectionInterface {

    void onConnected();

    void onFailed();
}